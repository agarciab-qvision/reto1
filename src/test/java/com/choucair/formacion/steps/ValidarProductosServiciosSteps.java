package com.choucair.formacion.steps;

import java.util.List;

import com.choucair.formacion.pageobjects.PrincipalPage;

import net.thucydides.core.annotations.Step;

public class ValidarProductosServiciosSteps {
	
	PrincipalPage principalPage;
	
	@Step
	public void ingreso() {
		
		principalPage.open();
		principalPage.VerificarIngresoSitio();
	
	}

	
	public void ingresoProductosServicios() {
		
		principalPage.VerificarProductosServicios();

	}

	
	public void ingresoTarjetaCredito() {
		
		principalPage.VerificarTarjetaCredito();;

	}
	
	public void mostrarEnConsola() {
		
		principalPage.MostrarConsolaAmericanExpressGreen();
		principalPage.MostrarConsolaMasterCardBlack();

	}
	
	
	public void solicitarTarjetaCreditoAmericanExpress() {
		principalPage.SolicitarTarjetaCreditoAmericanExpress();
		
	}
	
	
	public void ingresoSolicitudTarjetaDeCredito() {
		
		principalPage.VerificarIngresoSolicitudTarjetaDeCredito();

	}
	
	public void diligenciarInformacion(List<List<String>> data, int id) {

		principalPage.IngresarFrame();
		
		principalPage.nombres(data.get(id).get(0).trim());
		principalPage.apellidos(data.get(id).get(1).trim());
		principalPage.tipoDocumentoIdentidad(data.get(id).get(2).trim());
		principalPage.numeroDocumentoIdentidad(data.get(id).get(3).trim());
		principalPage.fechaNacimiento(data.get(id).get(4).trim());
		principalPage.ingresosMensuales(data.get(id).get(5).trim());
		principalPage.ciudadResidencia(data.get(id).get(6).trim());

		principalPage.ContinuarDiligenciarInformacionPaso1();
	}
	
	public void ingresoSolicitudTarjetaDeCreditoPaso2() {
		principalPage.verificarIngresoDiligenciarInformacionPaso2();
	}
}
