package com.choucair.formacion.definition;

import java.util.List;

import com.choucair.formacion.pageobjects.PrincipalPage;

import com.choucair.formacion.steps.ValidarProductosServiciosSteps;

import cucumber.api.DataTable;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import net.thucydides.core.annotations.Steps;

public class ValidarProductosServiciosDefinition {
	
	@Steps
	ValidarProductosServiciosSteps validarProductosServiciosSteps;
	
	
	
	@Steps
	PrincipalPage principalPage;
	
	@Given("^Ingresar al sitio principal$")
	public void ingresar_al_sitio_principal() {
		validarProductosServiciosSteps.ingreso();
	}
		
	@Given("^Ingresar a la opción productos y servicios de esta$")
	public void ingresar_a_la_opción_productos_y_servicios_de_esta()  {
		validarProductosServiciosSteps.ingresoProductosServicios();		
		
	}

	@Given("^Ingresar a la opción  Tarjetas de Credito$")
	public void ingresar_a_la_opción_Tarjetas_de_Credito() {
		validarProductosServiciosSteps.ingresoTarjetaCredito();
	}

	
	@Given("^Mostrar por Consola la información completa de la tarjeta de crédito American Express Green y MasterCard Black\\.$")
	public void mostrar_por_Consola_la_información_completa_de_la_tarjeta_de_crédito_American_Express_Green_y_MasterCard_Black()  {
		
		validarProductosServiciosSteps.mostrarEnConsola();
	}

	@Given("^Culminado el punto (\\d+), dar clic en el botón “Solicítala aquí” de la tarjeta American Express\\.$")
	public void culminado_el_punto_dar_clic_en_el_botón_Solicítala_aquí_de_la_tarjeta_American_Express(int arg1)  {
		
		validarProductosServiciosSteps.solicitarTarjetaCreditoAmericanExpress();
		

	}
	
	@When("^Tarjeta credito AEG Diligenciar informacion del formulario PasoUno$")
	public void tarjeta_credito_AEG_Diligenciar_informacion_del_formulario_PasoUno(DataTable dtDatosForm)  {
			
		
		List<List<String>> data = dtDatosForm.raw();
		
		for (int i=1; i <data.size(); i++) {
			validarProductosServiciosSteps.diligenciarInformacion(data, i);
		}

	}

	
	@Then("^Finalizar solicitud Continuar\\.$")
	public void finalizar_solicitud_Continuar()  {
		validarProductosServiciosSteps.ingresoSolicitudTarjetaDeCreditoPaso2();
	}

}
