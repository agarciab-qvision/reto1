package com.choucair.formacion.pageobjects;

import static org.junit.Assert.assertThat;
import static org.hamcrest.Matchers.containsString;

import net.serenitybdd.core.annotations.findby.FindBy;
import net.serenitybdd.core.pages.PageObject;
import net.serenitybdd.core.pages.WebElementFacade;
import net.thucydides.core.annotations.DefaultUrl;

@DefaultUrl("https://www.grupobancolombia.com/wps/portal/personas")

public class PrincipalPage extends PageObject {
	
	@FindBy(xpath="//*[@id=\'main-content\']/div[1]/div/div[4]/div/div/div/a")
	public WebElementFacade btnDescubreMas;

	
	@FindBy(xpath="//*[@id=\'productosPersonas\']/div/div[1]/div/h2")
	public WebElementFacade lblProductosServicios;
	
	@FindBy(xpath="//*[@id=\'main-menu\']/div[2]/ul[1]/li[3]/a")
	public WebElementFacade btnProductosServicios;
	
	
	@FindBy(xpath="//*[@id=\'main-content\']/div[1]/div[1]/div/div[2]/div/div[1]/h2")
	public WebElementFacade lblTarjetaCredito;
	
	@FindBy(xpath="//*[@id=\'productosPersonas\']/div/div[1]/div/div/div[7]/div/a")
	public WebElementFacade btnTarjetaCredito;
	
	
	// Captura de información para enviar por consola American Express Green

	@FindBy(xpath="//*[@id=\'card_0\']/div[2]/h2")
	public WebElementFacade lblhdAmericanExpressGreen;

	@FindBy(xpath="//*[@id=\'card_0\']/div[3]")
	public WebElementFacade lbldscAmericanExpressGreen;
	
	// Captura de información para enviar por consola Master Card Black
	
	@FindBy(xpath="//*[@id=\'card_1\']/div[2]/h2")
	public WebElementFacade lblhdMasterCardBlack;

	@FindBy(xpath="//*[@id=\'card_1\']/div[3]/ul")
	public WebElementFacade lbldscMasterCardBlack;
	
	@FindBy(xpath="//*[@id=\'card_0\']/div[4]/a")
	public WebElementFacade btnSolicitalAqui;
	
	@FindBy(xpath="//*[@id=\'div_descripcion\']/div/div/div/h1")
	public WebElementFacade lblSolicitudlTarjetaDeCredito;

	@FindBy(xpath="//*[@id=\'conIframe_rm\']/div/form/div[7]/div[2]/button")
	public WebElementFacade btnContinuarSTDCAEG;
	
	@FindBy(xpath="//*[@id=\'conIframe_ip\']/div/form/div[1]/div/h2")
	public WebElementFacade lblDiligenciamientoInformacionPaso2;	
	

	
	//
		
	public void VerificarIngresoSitio() {
		String lblVerificacion = "Descubre más";
		String strMensaje = btnDescubreMas.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
		
	}
	
	
	//Verificar Ingreso a diligenciamiento de informacion Paso2
	
	public void verificarIngresoDiligenciarInformacionPaso2() {
		String lblVerificacion = "Datos personales";
		String strMensaje = lblDiligenciamientoInformacionPaso2.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
	}
	
	
	
	
	public void VerificarProductosServicios() {
		btnProductosServicios.click();
		String lblVerificacion = "Productos y Servicios";
		String strMensaje = lblProductosServicios.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
		
	}
	
	public void VerificarTarjetaCredito() {
		btnTarjetaCredito.click();
		String lblVerificacion = "Escoge la tarjeta perfecta para cada momento";
		String strMensaje = lblTarjetaCredito.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
	}
	
	
	public void VerificarIngresoSolicitudTarjetaDeCredito() {
		lblSolicitudlTarjetaDeCredito.click();
		String lblVerificacion = "Solicitud Tarjeta de Crédito";
		String strMensaje = lblSolicitudlTarjetaDeCredito.getText();
		assertThat(strMensaje, containsString(lblVerificacion));
	}
	 
// Metodo para acceder al Frame
	
	public void IngresarFrame() {
		getDriver().switchTo().frame("Demos");
	}

	
	
	// Mostrar por consola información Tarjeta crédito American Express Green
	
	public void MostrarConsolaAmericanExpressGreen() {
		
		String hdAmericanExpressGreen = lblhdAmericanExpressGreen.getText();
		String dscAmericanExpressGreen = lbldscAmericanExpressGreen.getText();
		System.out.println(hdAmericanExpressGreen +"\n" + dscAmericanExpressGreen);
	}
	
	public void MostrarConsolaMasterCardBlack() {
		
		String hdAmericanExpressGreen = lblhdMasterCardBlack.getText();
		String dscAmericanExpressGreen = lbldscMasterCardBlack.getText();
		System.out.println(hdAmericanExpressGreen +"\n" + dscAmericanExpressGreen);
	}	
	
	public void SolicitarTarjetaCreditoAmericanExpress() {
		btnSolicitalAqui.click();
				
	}
	
	
	public void ContinuarDiligenciarInformacionPaso1() {
		
		btnContinuarSTDCAEG.click();
	}
	
	// Objetos formato solicitud - Paso1
	
		@FindBy(xpath="//*[@id=\'nombresReq\']")
		public WebElementFacade txtNombresSolicitante;
		
		@FindBy(xpath="//*[@id=\'apellidosReq\']")
		public WebElementFacade txtApellidosSolicitante;
		
		@FindBy(xpath="//*[@id=\'typedocreq\']")
		public WebElementFacade cmbTipoDocumentoSolicitante;
		
		@FindBy(xpath="//*[@id=\'numeroDocumento\']")
		public WebElementFacade txtTNumeroDocumentoSolicitante;
		
		@FindBy(xpath="//*[@id=\'fechaNacimientoReq\']")
		public WebElementFacade txtFechaNacimientoSolicitante;
		
		@FindBy(xpath="//*[@id=\'ingresos-mensuales\']")
		public WebElementFacade txtIngresosMensualesSolicitante;

		@FindBy(xpath="//*[@id=\'reqCiuidadDpto_value\']")
		public WebElementFacade txtCiudadResidenciaSolicitante;
		
		@FindBy(xpath="//*[@id=\'reqCiuidadDpto_dropdown\']/div[3]/div")
		public WebElementFacade txtCiudadResidenciaSolicitante2;		
	
		
		
	
	// Métodos para interactuar con cada uno de los Ttos
	
	public void nombres(String datoFormulario) {
		txtNombresSolicitante.click();
		txtNombresSolicitante.clear();
		txtNombresSolicitante.sendKeys(datoFormulario);
	}
	
	public void apellidos(String datoFormulario) {
		txtApellidosSolicitante.click();
		txtApellidosSolicitante.clear();
		txtApellidosSolicitante.sendKeys(datoFormulario);
	}

	public void tipoDocumentoIdentidad(String datoFormulario) {
		cmbTipoDocumentoSolicitante.click();
		cmbTipoDocumentoSolicitante.selectByVisibleText(datoFormulario);
	}
	
	public void numeroDocumentoIdentidad(String datoFormulario) {
		txtTNumeroDocumentoSolicitante.click();
		txtTNumeroDocumentoSolicitante.clear();
		txtTNumeroDocumentoSolicitante.sendKeys(datoFormulario);
	}
	
	public void fechaNacimiento(String datoFormulario) {
		txtFechaNacimientoSolicitante.click();
		txtFechaNacimientoSolicitante.clear();
		txtFechaNacimientoSolicitante.sendKeys(datoFormulario);
	}
	
	public void ingresosMensuales(String datoFormulario) {
		txtIngresosMensualesSolicitante.click();
		txtIngresosMensualesSolicitante.clear();
		txtIngresosMensualesSolicitante.sendKeys(datoFormulario);
	}
	
	public void ciudadResidencia(String datoFormulario) {
		txtCiudadResidenciaSolicitante.click();
		txtCiudadResidenciaSolicitante.clear();
		txtCiudadResidenciaSolicitante.sendKeys(datoFormulario);
		txtCiudadResidenciaSolicitante2.click();
	}
	
	

}
