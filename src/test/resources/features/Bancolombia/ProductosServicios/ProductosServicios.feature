#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@Regresion
Feature: Solicitud de tarjeta de crédito
					El usuario ingresa a la url https://www.grupobancolombia.com/wps/portal/personas 
					Accede a la opción: Productos y servicios.
					Se observa por consola información completa de tarjetas de crédito American Express Green y MasterCard Black.
					Solicita la tarjeta de crédito American Express.
					Diligencia todos los campos del formulario “Solicitud de tarjeta de crédito”.
					Envía la solicitud dando click.


@CasoExitoso
Scenario: Diligenciamiento exitoso de formulario para solicitud de tarjeta de credito

Given Ingresar al sitio principal
And Ingresar a la opción productos y servicios de esta 
And Ingresar a la opción  Tarjetas de Credito
And Mostrar por Consola la información completa de la tarjeta de crédito American Express Green y MasterCard Black.
And Culminado el punto 3, dar clic en el botón “Solicítala aquí” de la tarjeta American Express. 
When Tarjeta credito AEG Diligenciar informacion del formulario PasoUno
| Nombres   | Apellidos     | Documento identidad  | Numero documento identidad | Fecha nacimiento | Ingresos mensuales | Ciudad residencia    |
| Alexander | Garcia Berrio | Cédula de Ciudadanía | 71213135                   | 1978-10-09       | 1000000            | Medellin - Antioquia |
Then Finalizar solicitud Continuar. 

 # @tag2
 # Scenario Outline: Title of your scenario outline
 #   Given I want to write a step with <name>
 #   When I check for the <value> in step
 #   Then I verify the <status> in step

 #   Examples: 
 #     | name  | value | status  |
 #     | name1 |     5 | success |
 #     | name2 |     7 | Fail    |
